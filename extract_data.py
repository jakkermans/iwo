#!/usr/bin/python3
import sys
import re
import glob

def get_nni(textfile):
    data_lines = []
    countries = []
    with open(textfile, 'r') as f:
        for line in f:
            line = line.rstrip()
            split_line = line.split()
            for i in range(len(split_line)):
                if split_line[i] == ":":
                    split_line[i] = '0'
            data_line = [item for item in split_line if item not in ["p", "b"]]
            if len(data_line) == 28:
                if data_line[0][-2:] not in ["CH", "19", "28"]:
                    data_lines.append((data_line[0], data_line[11:]))
                    countries.append(data_line[0][-2:])

    nni_averages = []
    for line in data_lines[1:]:
        total_sum = 0
        data = line[1]
        for item in data:
            total_sum += int(item)
        average = total_sum / len(data)
        nni_averages.append((line[0], average))

    return nni_averages, countries

def get_hpi(textfile, countries):
    lines = []
    with open(textfile, 'r') as f:
        for line in f:
            line = line.rstrip()
            split_line = line.split()
            for i in range(len(split_line)):
                if split_line[i] == ":":
                    split_line[i] = "0"
            if split_line[0][-2:] in countries:
                country = split_line[0]
                data_line = [item for item in split_line if item not in ["e", "p", "b", "bp"]]
                lines.append((country, data_line[1:]))

    hpi_averages = []
    for line in lines[1:]:
        total_sum = 0
        data = line[1]
        for item in data:
            total_sum += float(item)
        average = total_sum / len(data)
        hpi_averages.append((line[0], average))

    return hpi_averages
def main():
    files = glob.glob("*.txt")
    nni_file = files[2]
    hpi_file = files[1]
    nni_average = hpi_average = 0
    hf = hs = lf = ls = 0
    nni, countries = get_nni(nni_file)
    hpi = get_hpi(hpi_file, countries)
    for item in nni:
        nni_average += item[1]
    global_nni_average = nni_average / len(nni)
    for data_item in hpi:
        hpi_average += data_item[1]
    global_hpi_average = hpi_average / len(hpi)

    for i in range(len(nni)):
        if nni[i][1] > global_nni_average:
            if hpi[i][1] > global_hpi_average:
                hf += 1
            else:
                hs += 1
        else:
            if hpi[i][1] > global_hpi_average:
                lf += 1
            else:
                ls += 1

    print("{0:>55}".format("National income"))
    print("{0:33} {1:^15} {2:^15}".format(" ", "high", "low"))
    print("{0:<15} {1:^15} {2:^15} {3:^15}".format("House price index", "fast", hf, lf))
    print("{0:<17} {1:^15} {2:^15} {3:^15}".format(" ", "slow", hs, ls))
if __name__ == "__main__":
    main()
