#!/bin/bash

TEXT=$1

if  [ -z "$TEXT" ]
then
    echo "Specify a file."
    exit
fi

FREQUENCY=`cat $TEXT | grep -oiw 'de' | wc -l`

echo Number of the word \"de\" \: $FREQUENCY
