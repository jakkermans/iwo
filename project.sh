#!/bin/bash

DATA1=`./extract_data.py | grep -o 'National income'`
DATA2=`./extract_data.py | grep -o 'high'`
DATA3=`./extract_data.py | grep -ow 'low'`
DATA4=`./extract_data.py | grep -o 'House price index'`
DATA5=`./extract_data.py | grep -o 'fast'`
DATA6=`./extract_data.py | grep -o '1' | head -1`
DATA7=`./extract_data.py | grep -o '2'`
DATA8=`./extract_data.py | grep -o 'slow'`
DATA9=`./extract_data.py | grep -o '1' | head -1`
DATA10=`./extract_data.py | grep -o '6'`

#In the data file, 'DE' is the abbreviation for Germany and 'EE' is the abbreviation for Estonia'.
echo "                            "$DATA1 > data.txt
echo "                              "$DATA2"     "$DATA3>> data.txt
echo $DATA4"      "$DATA5"     "$DATA6"       "$DATA7 >> data.txt
echo "                       "$DATA8"     "$DATA9"       "$DATA10 >> data.txt  
